import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {ProviderListComponent} from './provider-list/provider-list.component';
import {MoocListComponent} from './mooc-list/mooc-list.component';
import {MoocItemComponent} from './mooc-item/mooc-item.component';
import {ProviderItemComponent} from './provider-item/provider-item.component';
import {MoocsService} from './services/moocs.service';
import {ProvidersService} from './services/providers.service';
import {HttpClientModule} from '@angular/common/http';
import {SearchPipe} from './pipes/search.pipe';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    ProviderListComponent,
    MoocListComponent,
    MoocItemComponent,
    ProviderItemComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    MoocsService,
    ProvidersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
