import {Component, OnInit} from '@angular/core';
import {ProvidersService} from '../services/providers.service';

@Component({
  selector: 'app-provider-list',
  templateUrl: './provider-list.component.html',
  styleUrls: ['./provider-list.component.css']
})
export class ProviderListComponent implements OnInit {

  private providers: any;

  constructor(private ps: ProvidersService) {
  }

  ngOnInit() {
    this.ps.getAll().subscribe(
      (response) => {
        this.providers = response;
      }
    );
  }

}
