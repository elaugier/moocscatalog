import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-provider-item',
  templateUrl: './provider-item.component.html',
  styleUrls: ['./provider-item.component.css']
})
export class ProviderItemComponent implements OnInit {

  @Input('name') name;
  @Input('description') description;
  @Input('urlImage') urlImage;
  @Input('url') url;

  constructor() {
  }

  ngOnInit() {
  }

  goUrl(url: string) {
    console.log(url);
  }
}
