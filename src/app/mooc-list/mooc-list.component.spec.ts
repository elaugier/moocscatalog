import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MoocListComponent} from './mooc-list.component';

describe('MoocListComponent', () => {
  let component: MoocListComponent;
  let fixture: ComponentFixture<MoocListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MoocListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoocListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
