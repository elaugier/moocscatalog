import {Component, OnInit} from '@angular/core';
import {MoocsService} from '../services/moocs.service';

@Component({
  selector: 'app-mooc-list',
  templateUrl: './mooc-list.component.html',
  styleUrls: ['./mooc-list.component.css']
})
export class MoocListComponent implements OnInit {

  private moocs: any;
  private query: string;

  constructor(private ms: MoocsService) {
  }

  ngOnInit() {
    this.ms.getAll().subscribe((response) => {
      this.moocs = response;
    });
  }

}
