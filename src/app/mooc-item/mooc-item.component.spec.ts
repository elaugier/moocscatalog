import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MoocItemComponent} from './mooc-item.component';

describe('MoocItemComponent', () => {
  let component: MoocItemComponent;
  let fixture: ComponentFixture<MoocItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MoocItemComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoocItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
