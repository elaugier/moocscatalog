import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-mooc-item',
  templateUrl: './mooc-item.component.html',
  styleUrls: ['./mooc-item.component.css']
})
export class MoocItemComponent implements OnInit {

  @Input('name') name;
  @Input('description') description;
  @Input('urlImage') urlImage;
  @Input('url') url;

  constructor() {
  }

  ngOnInit() {
  }

  goUrl(url: string) {
    console.log(url);
  }

}
