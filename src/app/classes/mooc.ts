export class Mooc {
  id?: number;
  name?: string;
  description?: string;
  urlImage?: string;
  url?: string;
}
