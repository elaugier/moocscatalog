export class Provider {
  id?: number;
  name?: string;
  description?: string;
  urlImage?: string;
  url?: string;
}
