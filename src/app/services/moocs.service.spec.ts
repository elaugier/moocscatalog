import {inject, TestBed} from '@angular/core/testing';

import {MoocsService} from './moocs.service';

describe('MoocsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MoocsService]
    });
  });

  it('should be created', inject([MoocsService], (service: MoocsService) => {
    expect(service).toBeTruthy();
  }));
});
