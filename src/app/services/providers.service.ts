import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Provider} from '../classes/provider';

@Injectable()
export class ProvidersService {
  public apiHost = './assets/providersData.json';

  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<Provider> {
    return this.http.get(this.apiHost);
  }
}
