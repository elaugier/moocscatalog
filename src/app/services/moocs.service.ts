import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Mooc} from '../classes/mooc';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class MoocsService {
  public apiHost = './assets/moocsData.json';

  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<Mooc> {
    return this.http.get(this.apiHost);
  }
}

